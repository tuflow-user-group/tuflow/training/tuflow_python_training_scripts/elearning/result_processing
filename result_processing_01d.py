import pytuflow
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import numpy as np
import pandas as pd
from pathlib import Path


INPUT = 'data/plot/M03_5m_CLA_001.tpc'
OUTPUT_FOLDER = 'output'


def main():
    res = pytuflow.ResData()
    err, msg = res.load(INPUT)
    error_handler(err, msg)

    for channel in res.channels():
        err, msg, (x, y) = res.getTimeSeriesData(channel, 'V')
        error_handler(err, msg)
        err, msg, max_ = res.maximum(channel, 'V')
        error_handler(err, msg)
        err, msg, tmax = res.timeOfMaximum(channel, 'V')
        error_handler(err, msg)
        if not is_stable(x, y, max_):
            fig, ax = plt.subplots()
            ax.plot(x, y, label=channel)
            ax.legend()
            output_name = Path(OUTPUT_FOLDER) / f'{channel}.png'
            fig.savefig(output_name)

    print('Finished')


def error_handler(err, msg):
    if err:
        exit(msg)

def is_stable(x, y, maxy):
    if min(y) < 0:
        return False

    maxy_ts = max(y)
    if (maxy - maxy_ts) / maxy > 0.05:
        return False

    df = pd.DataFrame(np.transpose(np.array([x, y])))
    df['grad'] = df[1].diff() / df[0].diff()
    df['sign'] = np.sign(df['grad'])
    df['sign_dif'] = df['sign'].diff()
    df['sign_dif_dif'] = df['sign_dif'].diff()
    if np.any(abs(df['sign_dif_dif']) == 4.):
        return False

    return True


if __name__ == '__main__':
    main()
