from netCDF4 import Dataset
import matplotlib.pyplot as plt


INPUT = 'data/M03_5m_CLA_001.nc'


def main():
    with Dataset(INPUT, 'r') as nc:
        non_result_type_variables = ['time', 'static_time', 'x', 'y', 'crs']
        print('Available Result Types:')
        for variable in nc.variables:
            if variable not in non_result_type_variables:
                print(f'\t{variable}')

        # get maximum depth
        variable = 'maximum_depth'
        xloc = nc['x'][:]
        yloc = nc['y'][:]
        depth = nc[variable][0,:,:]  # result is 3D array (time, x, y) - this is slicing it at the first timestep

        # plot maximum depth
        fig, ax = plt.subplots()
        contour = ax.contourf(xloc, yloc, depth)
        # add colour bar
        cb = plt.colorbar(contour)
        cb.ax.set_xlabel('Depth (m)')
        # show plot
        plt.show()

    print('\nFinished')


if __name__ == '__main__':
    main()