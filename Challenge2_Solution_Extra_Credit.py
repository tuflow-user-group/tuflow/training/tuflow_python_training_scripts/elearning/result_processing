import pytuflow
import matplotlib.pyplot as plt
from pathlib import Path


INPUT_CLA = 'data/challenge_2/plot/M03_5m_CLA_001.tpc'
INPUT_HPC = 'data/challenge_2/plot/M03_5m_HPC_001.tpc'
OUTPUT_FOLDER = 'output/challenge_2'


def main():
    res_cla = pytuflow.ResData()
    res_hpc = pytuflow.ResData()
    err, msg = res_cla.load(INPUT_CLA)
    error_handler(err, msg)
    err, msg = res_hpc.load(INPUT_HPC)
    error_handler(err, msg)

    for channel in res_cla.channels():
        # time series
        err, msg, (time_cla, flow_cla) = res_cla.getTimeSeriesData(channel, 'Q')
        error_handler(err, msg)
        err, msg, (time_hpc, flow_hpc) = res_hpc.getTimeSeriesData(channel, 'Q')
        error_handler(err, msg)
        # maximum
        err, msg, max_cla = res_cla.maximum(channel, 'Q')
        error_handler(err, msg)
        err, msg, max_hpc = res_hpc.maximum(channel, 'Q')
        error_handler(err, msg)

        # extra credit time of maximum - use for positioning label on plot
        err, msg, tmax_cla = res_cla.timeOfMaximum(channel, 'Q')
        error_handler(err, msg)
        err, msg, tmax_hpc = res_hpc.timeOfMaximum(channel, 'Q')
        error_handler(err, msg)

        # difference
        diff = abs(max_cla - max_hpc)
        print(f'Difference {channel}: {diff:.02f}')

        # plot
        fig, ax = plt.subplots()
        ax.plot(time_cla, flow_cla, label=f'{channel} - cla')
        ax.plot(time_hpc, flow_hpc, label=f'{channel} - hpc')
        ax.grid()
        ax.set_xlabel('Time (hr)')
        ax.set_ylabel('Flow (m$^3$/s)')

        # extra credit add_label
        # put label at (x, y) coordinates (time of max, maximum of cla and hpc)
        add_label(ax, f'Diff in Max: {diff:.02f}', tmax_cla, max(max_cla, max_hpc))

        ax.legend(loc='upper left')  # extra credit, position legend top left so it doesn't clip our label
        output_name = Path(OUTPUT_FOLDER) / f'{channel}.png'
        fig.savefig(output_name)


def error_handler(err, msg):
    if err:
        exit(msg)


def add_label(ax, text, xpos, ypos):
    ypos = ypos * 1.05  # move ypos up by 5% so it's not clipping the lines
    ax.annotate(text, xy=(xpos, ypos), bbox=dict(facecolor='white', edgecolor='black', pad=2))

    # re-limit the y axis so it contains the label
    ylim = ax.get_ylim()
    ylim = (ylim[0], ylim[1] * 1.1)
    ax.set_ylim(ylim)


if __name__ == '__main__':
    main()