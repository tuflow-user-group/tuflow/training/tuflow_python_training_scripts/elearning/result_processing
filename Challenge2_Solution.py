import pytuflow
import matplotlib.pyplot as plt
from pathlib import Path


INPUT_CLA = 'data/challenge_2/plot/M03_5m_CLA_001.tpc'
INPUT_HPC = 'data/challenge_2/plot/M03_5m_HPC_001.tpc'
OUTPUT_FOLDER = 'output/challenge_2'


def main():
    res_cla = pytuflow.ResData()
    res_hpc = pytuflow.ResData()
    err, msg = res_cla.load(INPUT_CLA)
    error_handler(err, msg)
    err, msg = res_hpc.load(INPUT_HPC)
    error_handler(err, msg)

    for channel in res_cla.channels():
        # time series
        err, msg, (time_cla, flow_cla) = res_cla.getTimeSeriesData(channel, 'Q')
        error_handler(err, msg)
        err, msg, (time_hpc, flow_hpc) = res_hpc.getTimeSeriesData(channel, 'Q')
        error_handler(err, msg)
        # maximum
        err, msg, max_cla = res_cla.maximum(channel, 'Q')
        error_handler(err, msg)
        err, msg, max_hpc = res_hpc.maximum(channel, 'Q')
        error_handler(err, msg)

        # difference
        diff = abs(max_cla - max_hpc)
        print(f'Difference {channel}: {diff:.02f}')

        # plot
        fig, ax = plt.subplots()
        ax.plot(time_cla, flow_cla, label=f'{channel} - cla')
        ax.plot(time_hpc, flow_hpc, label=f'{channel} - hpc')
        ax.grid()
        ax.set_xlabel('Time (hr)')
        ax.set_ylabel('Flow (m$^3$/s)')
        ax.legend()
        output_name = Path(OUTPUT_FOLDER) / f'{channel}.png'
        fig.savefig(output_name)


def error_handler(err, msg):
    if err:
        exit(msg)


if __name__ == '__main__':
    main()