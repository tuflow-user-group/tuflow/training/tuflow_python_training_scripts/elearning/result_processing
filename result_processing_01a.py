import pytuflow


INPUT = 'data/plot/M03_5m_CLA_001.tpc'


def main():
    res = pytuflow.ResData()
    err, msg = res.load(INPUT)
    error_handler(err, msg)

    print('Finished')


def error_handler(err, msg):
    if err:
        exit(msg)


if __name__ == '__main__':
    main()